# необходимые модули
from time import sleep


class Hero:
    """Общий класс для героев
    name - имя, строка
    health - здоровье, число
    armor - броня, число
    power - сила удара, число
    weapon - оружие, строка
    """

    # конструктор класса, создает экземпляр
    def __init__(self, name, health, armor, power, weapon):
        self.name = name
        self.health = health
        self.armor = armor
        self.power = power
        self.weapon = weapon

    # вывод информации о персонаже
    def print_info(self):
        print("Поприветствуйте героя ->", self.name)
        print("Уровень здоровья:", self.health)
        print("Класс брони:", self.armor)
        print("Сила удара:", self.power)
        print("Оружие:", self.weapon, "\n")

    # удар по другому персонажу
    def strike(self, enemy):
        # вывод информации об ударе
        print(
            "->УДАР! "
            + self.name
            + " атакует "
            + enemy.name
            + " с силой "
            + str(self.power)
            + ", используя "
            + self.weapon
            + "\n"
        )

        # сам удар
        # сначала вся сила удара наносится по броне противника
        # если броня уходит в минус (становится меньше нуля)
        # то этот минус прибавляется к здоровью
        enemy.armor -= self.power
        if enemy.armor < 0:
            enemy.health += enemy.armor
            enemy.armor = 0

        # вывод информации о противнике
        print(
            enemy.name
            + " покачнулся.\nКласс его брони упал до "
            + str(enemy.armor)
            + ", а уровень здоровья до "
            + str(enemy.health)
            + "\n"
        )

    # запуск сражения между двуми персонажами
    def fight(self, enemy):
        # сражение идет до тех пор,
        # пока оба персонажа живы
        while self.health > 0 and enemy.health > 0:
            # сначала персонаж 1 атакует персонажа 2
            self.strike(enemy)
            # проверяем здоровье персонажа 2
            if enemy.health <= 0:
                print(enemy.name, " пал в этом бою!\n")
                break
            # делаем паузу
            sleep(5)

            # затем персонаж 2 атакует персонажа 1
            enemy.strike(self)
            # проверяем здоровье персонажа 1
            if self.health <= 0:
                print(self.name, " пал в этом бою!\n")
                break
            # делаем паузу
            sleep(5)
